package OOPCa2AidanBligh;

/**
 * Adding javadoc breaks this class for some reason
 * @author Aidan Bligh D00219125
 */
import java.util.ArrayList;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Bird extends Animal{
    private Scanner kb = new Scanner(System.in);
    private double wingspan;
    private boolean ableToFly;

    public Bird(double wingspan, boolean ableToFly, String name, String breed, int age, String[] color) {
        super(name, breed, age, color);
        this.wingspan = wingspan;
        this.ableToFly = ableToFly;
    }
    
    //empty constuctor for composition

    public Bird() {
    }
    
    public double addWingspan()
    {
        
        //boolean done = false;
        boolean done = false;
        while(!done)
        {
            try
            {
                kb.reset();
                System.out.println("Enter yor birds wingspan");
                double wingspan = kb.nextDouble();
                if(wingspan > 0)
                {
                    done = true;
                }
            }
            catch(InputMismatchException e)
            {
                System.out.println("Wow you're really trying to break this");
            }
            //done = true;
        }
        return wingspan;
    }
    
    
    public boolean ableToFly()
    {
        
        boolean done = false;
        boolean fly = false;
        while(!done)
        {
            try
            {
                kb.reset();
                System.out.println("is your bird able to fly? Enter Y/N");
                String answer = kb.nextLine();//don't know why but this stop above message from appearing twice
                answer = kb.nextLine();
                if(answer.equalsIgnoreCase("y"))
                {
                    fly = true;
                    done = true;
                }
                else if(answer.equalsIgnoreCase("n"))
                {
                    fly = false;
                    done = true;
                }
//                else
//                {
//                    System.out.println("Plean enter y or n");
//                }
            }
            catch(InputMismatchException e)
            {
                System.out.println("Stop trying to break my program by putting in the wrong input");
            }
        }
        return fly;
    }

        public void editBird(Bird b)
    {
        //menu to ask what values hneed changing
        try
        {
            int choice = 0;
            while (choice!=6)
            {
                //add enums
                printEditingOptions();
                switch(choice)
                {
                    case 1:
                        changeName(b);
                        break;
                    case 2:
                        changeAge(b);
                        break;
                    case 3:
                        changeBreed(b);
                        break;
                    case 4:
                        changeColors(b);
                        break;
                    case 5:
                        changeWingspan(b);
                        break;
                    case 6:
                        changeFlyingAbility(b);
                }
                
            }
        }
        catch(InputMismatchException e){}
    }
        
        
    private void printEditingOptions()
    {
        System.out.println("Enter 1 to change the birds name.");
        System.out.println("Enter 2 to change the birds age.");
        System.out.println("Enter 3 to change the birds breed.");
        System.out.println("Enter 4 to change the birds color.");
        System.out.println("Enter 5 to change the birds wingspan.");
        System.out.println("Enter 6 to change the birds flying ability status");
        System.out.println("Enter 6 to quit.");
    }
    
    
    private void changeName(Bird b)
    {
        System.out.println("What would you like to rename " + b.getName() + "?");
        String name = kb.nextLine();
        b.setName(name);
        System.out.println("Bird renamed");
    }
    
    private void changeAge(Bird b)
    {
        System.out.println("What age would you like to set for "+ b.getName() +"?");
        int age = kb.nextInt();
        b.setAge(age);
        System.out.println("new age set");
    }
    
    private void changeBreed(Bird b)
    {
        System.out.println("What breed would you like to set "+b.getName() + " as?");
        String breed = kb.nextLine();
        b.setBreed(breed);
        System.out.println("New breed set");
    }
    
    private void changeColors(Bird b)
    {
        String [] color = addColors();
        b.setColor(color);
        System.out.println("colors changed");
    }
    
    private void changeWingspan(Bird b)
    {
        System.out.println("What would you like to set "+b.getName()+"'s wingspan as(use decimal points)");
        double wingspan = kb.nextDouble();
        b.setWingspan(wingspan);
        System.out.println("Wingspan changed");
    }
    
    private void changeFlyingAbility(Bird b)
    {
        boolean flying = ableToFly();
        b.setAbleToFly(ableToFly);
        System.out.println("Flying ability changed");
    }
    
    public boolean checkIsCopy(Bird b, ArrayList<Bird> birdList)
    {
        boolean isCopy = false;
        for(int i = 0; i>birdList.size();i++)
        {
            int similarities = 0;
            if(b.getName().equalsIgnoreCase(birdList.get(i).getName()))
            {
                similarities++;
            }
            if(b.getBreed().equalsIgnoreCase(birdList.get(i).getBreed()))
            {
                similarities++;
            }
            if(b.getAge() == birdList.get(i).getAge())
            {
                similarities++;
            }
            if(b.getWingspan() == birdList.get(i).getWingspan())
            {
                similarities++;
            }
            if(b.isAbleToFly() == birdList.get(i).isAbleToFly())
            {
                similarities++;
            }
            if(similarities>3)
            {
                isCopy = true;
            }
        }
        return isCopy;
    }
    
    @Override
    public String toString() {
        return "Type: Bird" +  "\nname: " + getName() +"\nwingspan: " + wingspan
                + "\nableToFly: " + ableToFly +"\nbreed: " + getBreed() 
                + "\nage: " + getAge() + "\ncolor: " + Arrays.deepToString(getColor()) + "\npetId: " 
                + getPetId()/*+ "\nDate Created: " + getDateAndTimeCreated()*/;
    }

    private void setWingspan(double wingspan) {
        this.wingspan = wingspan;
    }

    private void setAbleToFly(boolean ableToFly) {
        this.ableToFly = ableToFly;
    }

    public double getWingspan() {
        return wingspan;
    }

    public boolean isAbleToFly() {
        return ableToFly;
    }

    public void birdStats(ArrayList<Owner> owners, ArrayList<Bird> birds) 
    {
        int birdCount = 0;
        int flyableCount = 0;
        for(int i = 0; i<owners.size();i++)
        {
           for(int j = 0; j<owners.get(i).getPets().size();j++)
           {
               if(owners.get(i).getPets().get(j).toString().contains("Type: Bird"))
               {
                   birdCount++;
                   if(owners.get(i).getPets().get(j).toString().contains("ableToFly= true"))
                   {
                       flyableCount++;
                   }
               }
           }
        }
        int amountRegistered = birdCount;
        birdCount += birds.size();
        for(int k = 0; k<birds.size();k++)
        {
            if(birds.get(k).isAbleToFly())
            {
                flyableCount++;
            }
        }
        double percentRegistered = ((double)amountRegistered/birdCount)*100;
        double percentFlyable = ((double) flyableCount/birdCount)*100;
        System.out.println("There are "+birdCount+" birds on the system, "
        + amountRegistered +" or "+percentRegistered+ "% of them are registered to an owner\n"
        + flyableCount + " or "+ percentFlyable+ "% birds are able to fly.");
    }
    
}