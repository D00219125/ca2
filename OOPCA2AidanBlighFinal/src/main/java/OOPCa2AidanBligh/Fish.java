package OOPCa2AidanBligh;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;
/**
 *
 * @author Aidan Bligh D00219125
 */
public class Fish extends Animal{
    private Scanner kb = new Scanner(System.in);
    private String waterType;
    
    /**
     *Constructor for fish class
     * @param waterType
     * @param name
     * @param breed
     * @param age
     * @param color
     */
    public Fish(String waterType, String name, String breed, int age, String[] color) {
        super(name, breed, age, color);
        this.waterType = waterType;
    }
    

    /**
     * empty constructor for composition
     */
    public Fish() {
    }
    
    /**
     *Used to add a fishes  waterType used before constructor
     * @return
     */
    public String addWaterType()
    {
        String waterType = "";
        boolean done = false;
        while(!done)
        {
            try
            {
                //add enums
                System.out.println("Is your fish 1: freshwater 2: seawater 3: brackish? Please enter the corrisponding number");
                int answer = kb.nextInt();
                if(answer == 1)
                {
                    waterType = "Freshwater";
                    done = true;
                }
                else if(answer == 2)
                {
                    waterType = "Seawater";
                    done = true;
                }
                else if(answer == 3)
                {
                    waterType = "Brackish";
                    done = true;
                }
                else
                {
                    System.out.println("Please enter a number corrisponding to your fishes water type");
                }
            }
            catch(InputMismatchException e)
            {
                System.out.println("Keep on trying you'll breaqk my code eventually");
            }
        }
        return waterType;
    }
    
    /**
     *Used to edit fish f
     * @param f
     */
    public void editFish(Fish f)
    {
        
        try
        {
            int choice = 0;
            while (choice!=6)
            {
                //add enums
                printEditingOptions();
                switch(choice)
                {
                    case 1:
                        changeName(f);
                        break;
                    case 2:
                        changeAge(f);
                        break;
                    case 3:
                        changeBreed(f);
                        break;
                    case 4:
                        changeColors(f);
                        break;
                    case 5:
                        changeWaterType(f);
                        break;
                }
                
            }
        }
        catch(InputMismatchException e){}
    }
    
    /*
     * @param f
     * @param fishList
     * @return returns true if fish f is a copy
     */
    public boolean checkIsCopy(Fish f, ArrayList<Fish> fishList)
    {
        boolean isCopy = false;
        for(int i = 0; i>fishList.size();i++)
        {
            int similarities = 0;
            if(f.getName().equalsIgnoreCase(fishList.get(i).getName()))
            {
                similarities++;
            }
            if(f.getBreed().equalsIgnoreCase(fishList.get(i).getBreed()))
            {
                similarities++;
            }
            if(f.getAge() == fishList.get(i).getAge())
            {
                similarities++;
            }
            if(f.getWaterType().equalsIgnoreCase(fishList.get(i).getWaterType()))
            {
                similarities++;
            }
            if(similarities>3)
            {
                isCopy = true;
            }
        }
        return isCopy;
    }
    
    
    private void printEditingOptions()
    {
        System.out.println("Enter 1 to change the fishes name.");
        System.out.println("Enter 2 to change the fishes age.");
        System.out.println("Enter 3 to change the fishes breed.");
        System.out.println("Enter 4 to change the fishes color.");
        System.out.println("Enter 5 to change the fishes water Type.");
        System.out.println("Enter 6 to quit.");
    }
    
    private void changeName(Fish f)
    {
        System.out.println("What would you like to rename " + f.getName() + "?");
        String name = kb.nextLine();
        f.setName(name);
        System.out.println("Fish renamed");
    }
    
    private void changeAge(Fish f)
    {
        System.out.println("What age would you like to set for "+ f.getName() +"?");
        int age = kb.nextInt();
        f.setAge(age);
        System.out.println("new age set");
    }
    
    private void changeBreed(Fish f)
    {
        System.out.println("What breed would you like to set "+f.getName() + " as?");
        String breed = kb.nextLine();
        f.setBreed(breed);
        System.out.println("New breed set");
    }
    
    private void changeColors(Fish f)
    {
        String [] color = addColors();
        f.setColor(color);
        System.out.println("colors changed");
    }
    
    private void changeWaterType(Fish f)
    {
        String newWaterType = addWaterType();
        f.setWaterType(waterType);
        System.out.println("new waterType set");
    }
    
    @Override
    public String toString() {
        return "Type: Fish" + "\nname: " + getName() + "\nwaterType: " + 
                waterType+"\nbreed= " + getBreed() + "\nage= " + getAge() + "\ncolor= " 
                + Arrays.deepToString(getColor()) + "\npetId= " + getPetId()/*"\nDate created" + getDateAndTimeCreated()*/ ;
    }

    private void setWaterType(String waterType) {
        this.waterType = waterType;
    }

    /**
     *
     * @return
     */
    public String getWaterType() {
        return waterType;
    }

    /**
     *generates mammal statistics, prints number of owned and un-owned pets 
     * @param owners
     * @param fishes
     */
    public void fishStats(ArrayList<Owner> owners, ArrayList<Fish> fishes) {
        int seawaterCount = 0;
        int freshwaterCount = 0;
        int brackishwaterCount = 0;
        int fishCount = 0;
        
        for(int i = 0; i<owners.size();i++)
        {
           for(int j = 0; j<owners.get(i).getPets().size();j++)
           {
               if(owners.get(i).getPets().get(j).toString().contains("Type: Fish"))
               {
                   fishCount++;
                   if(owners.get(i).getPets().get(j).toString().contains("waterType: Freshwater"))
                   {
                       freshwaterCount++;
                   }
                   if(owners.get(i).getPets().get(j).toString().contains("waterType: Seawater"))
                   {
                       seawaterCount++;
                   }
                   if(owners.get(i).getPets().get(j).toString().contains("waterType: Brackish"))
                   {
                       brackishwaterCount++;
                   }
               }
           }
        }
        int amountRegistered = fishCount;
        fishCount += fishes.size();
        for(int k = 0; k<fishes.size();k++)
        {
            if(fishes.get(k).getWaterType().equals("Freshwater"))
            {
                freshwaterCount++;
            }
            if(fishes.get(k).getWaterType().equals("Seawater"))
            {
                seawaterCount++;
            }
            if(fishes.get(k).getWaterType().equals("Brackish"))
            {
                brackishwaterCount++;
            }
        }
        double percentFresh = ((double) freshwaterCount/fishCount)*100;
        double percentSea = ((double) seawaterCount/fishCount)*100;
        double percentBrackish = ((double) brackishwaterCount/fishCount)*100;
        double percentRegistered = ((double) amountRegistered/fishCount)*100;
        System.out.println("There are "+fishCount+" Fish  on this system.\n"
        + freshwaterCount+ " of them are freshwater types.\n" + seawaterCount
                +" of them are seawater types.\n" + brackishwaterCount + 
                "of them are brackish water types\n In percentages\nFresh water = " +
                percentFresh+"%\nSea water = " + percentSea + "%\nBrackish water = " 
                +percentBrackish +"%\nThere are " + amountRegistered + 
                " fish registered out of the  " + fishCount + " on the system.\n"
                        + "Meaning "+percentRegistered +"% of fish are registered to an owner");
    }
    
    
    
}
