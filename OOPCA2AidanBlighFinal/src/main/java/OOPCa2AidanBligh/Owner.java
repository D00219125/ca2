package OOPCa2AidanBligh;

import java.util.ArrayList;
import java.util.UUID;

/**
 *
 * @author Aidan Bligh D00219125
 */
public class Owner {
    private ArrayList<Animal> pets = new ArrayList<Animal>();
    private String ownerName;
    private String ownerId;
    private String ownerEmail;
    private String ownerPhoneNo;
    private String ownerAddress;

    /**
     *constructor for owner
     * @param ownerName
     * @param ownerEmail
     * @param ownerPhoneNo
     * @param ownerAddress
     */
    public Owner(String ownerName, String ownerEmail, String ownerPhoneNo, String ownerAddress) {
        this.ownerName = ownerName;
        this.ownerEmail = ownerEmail;
        this.ownerPhoneNo = ownerPhoneNo;
        this.ownerAddress = ownerAddress;
        this.ownerId = UUID.randomUUID().toString();
    }

    /**
     *
     * @return
     */
    public String getOwnerName() {
        return ownerName;
    }

    /**
     *
     * @return
     */
    public String getOwnerEmail() {
        return ownerEmail;
    }
    
    /**
     *
     * @return
     */
    public ArrayList<Animal> getPets() {
        return pets;
    }

    /**
     *
     * @param pet
     */
    public void addToPets(Animal pet) {
        pets.add(pet);
    }
    
    

    @Override
    public String toString() {
        return "Owner{" + "pets=" + pets + ", ownerName=" + ownerName + ", ownerId=" + ownerId + ", ownerEmail=" + ownerEmail + ", ownerPhoneNo=" + ownerPhoneNo + ", ownerAddress=" + ownerAddress + '}';
    }
    
}
