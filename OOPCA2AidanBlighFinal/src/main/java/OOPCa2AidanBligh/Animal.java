package OOPCa2AidanBligh;

import java.util.Scanner;
import java.util.UUID;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collections;
import java.util.Objects;

/**
 *
 * @author Aidan Bligh D00219125
 */
public class Animal {
    private Scanner kb = new Scanner(System.in);
    private String name;
    private String breed;
    private int age;
    private String [] color;
    private String petId;
    private String dateAndTimeCreated;
    private LocalDateTime dateAndTime;
    private DateTimeFormatter dtf = DateTimeFormatter.ofPattern("DD MM YYYY");

    /**
     *Constructor for animal class that pets inherit from
     * @param name
     * @param breed
     * @param age
     * @param color
     */
    public Animal(String name, String breed, int age, String[] color) {
        this.name = name;
        this.breed = breed;
        this.age = age;
        this.color = color;
        this.petId = UUID.randomUUID().toString();
        //this.dateAndTimeCreated = dtf.format(dateAndTime);
    }
    
    
    

    /**
     *empty constructor for composition
     */

    public Animal() {
    }
    
    /**
     *Used to add color to an animal call during construction of every animal
     * @return
     */
    public String [] addColors()
    {
        
        try
        {
            System.out.println("How many colors does your pet have?");
            int size = -1;
            while(size<1)
            {
                size = kb.nextInt();
            }
            String [] colors = new String [size];
            for(int i = 0; i<size;i++)
            {
                System.out.println("Enter color "+ i+1);
                colors[i] = kb.next();
            }
            return colors;
        }
        //dont need this
        catch(NegativeArraySizeException n)
        {
            kb.next();
            System.out.println("Stooooop Iiiiiit!");
            
            addColors();
            return null;
        }
        
    }
    //getters for the toStrings in the animale type classes

    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @return
     */
    public String getBreed() {
        return breed;
    }

    /**
     *
     * @return
     */
    public int getAge() {
        return age;
    }

    /**
     *
     * @return
     */
    public String[] getColor() {
        return color;
    }

    /**
     *
     * @return
     */
    public String getPetId() {
        return petId;
    }
    //need one of these to print time

    /**
     *
     * @return
     */
    public LocalDateTime getDateAndTime() {
        return dateAndTime;
    }
    
    /**
     *
     * @return
     */
    public DateTimeFormatter getDtf() {
        return dtf;
    }

    /**
     *
     * @return
     */
    public String getDateAndTimeCreated() {
        return dateAndTimeCreated;
    }
    

    //setters

    /**
     *
     * @param name
     */

    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @param breed
     */
    public void setBreed(String breed) {
        this.breed = breed;
    }

    /**
     *sets age
     * @param age
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     *sets color
     * @param color
     */
    public void setColor(String[] color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Animal{" + "name=" + name + ", breed=" + breed + ", age=" + age + ", color=" + color + ", petId=" + petId + /*", dateAndTime=" + dateAndTimeCreated +*/ '}';
    }
    
//    public int compareTo(Animal a1, Animal a2)
//    {
//        return a1.getAge() - a2.getAge();
//    }

    //I think I need these for compare
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + Objects.hashCode(this.kb);
        hash = 31 * hash + Objects.hashCode(this.name);
        hash = 31 * hash + Objects.hashCode(this.breed);
        hash = 31 * hash + this.age;
        hash = 31 * hash + Arrays.deepHashCode(this.color);
        hash = 31 * hash + Objects.hashCode(this.petId);
        hash = 31 * hash + Objects.hashCode(this.dateAndTimeCreated);
        hash = 31 * hash + Objects.hashCode(this.dateAndTime);
        hash = 31 * hash + Objects.hashCode(this.dtf);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Animal other = (Animal) obj;
        if (this.age != other.age) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.breed, other.breed)) {
            return false;
        }
        if (!Objects.equals(this.petId, other.petId)) {
            return false;
        }
        if (!Objects.equals(this.dateAndTimeCreated, other.dateAndTimeCreated)) {
            return false;
        }
        if (!Objects.equals(this.kb, other.kb)) {
            return false;
        }
        if (!Arrays.deepEquals(this.color, other.color)) {
            return false;
        }
        if (!Objects.equals(this.dateAndTime, other.dateAndTime)) {
            return false;
        }
        if (!Objects.equals(this.dtf, other.dtf)) {
            return false;
        }
        return true;
    }

    
    
    
}
