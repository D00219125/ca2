package OOPCa2AidanBligh;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *JavaDoc wont generate
 * @author Aidan Bligh D00219125
 */


class PetRegistryMenu {
    //array lists for Storing 
    private ArrayList <Owner> owners = new ArrayList<>();
    private ArrayList <Bird> birds = new ArrayList<>();
    private ArrayList <Mammal> mammals = new ArrayList<>();
    private ArrayList <Fish> fishes = new ArrayList<>();
    //initialising classes for copmpisition
    private Animal animal = new Animal();
    private Mammal mammal = new Mammal();
    private Bird bird = new Bird();
    private Fish fish = new Fish();
    private Scanner kb = new Scanner(System.in);
    public void petMenu()
    {
        try
        {
            int choice = 0;
            while(choice != 7)
            {
                printOptions();
                choice = kb.nextInt();
                //use enums
                switch(choice)
                {
                    case 1: 
                        addPet();
                    break;
                    case 2: 
                        addOwner();
                    break;
                    case 3:
                        assignPet();
                        break;
                    case 4:
                        printingMenu();
                        break;
                    case 5:
                        generateStatistics();
                        break;
                    case 6: 
                        deletePet();
                    break;
                }
            }
        }
        catch(InputMismatchException e)
        {
            System.out.println("Go ahead I dare you to keep on trying to break this.");
            kb.next();
            petMenu();
        }
    }
    private void printOptions()
    {
        System.out.println("Enter 1 to add an animal.");
        System.out.println("Enter 2 to add an owner.");
        System.out.println("Enter 3 to assign an animal to an owner.");
        System.out.println("Enter 4 to view all pets in the system");
        System.out.println("Enter 5 to generate statistics");
        System.out.println("Enter 6 to delete pet");
        System.out.println("Enter 7 to quit.");
    }
    private void addPet()
    {
        
        boolean done = false;
        while(!done)
        {
            try
            {
                int choice = 0;
                System.out.println("Enter 1 if your animal is a mammal.");
                System.out.println("Enter 2 if your animal is a bird.");
                System.out.println("Enter 3 if your animal is a fish.");
                choice = kb.nextInt();
                //use enums
                switch(choice)
                {
                    case 1:
                        addMammal();
                        break;
                    case 2:
                        addBird();
                        break;
                    case 3:
                        addFish();
                        break;
                }
                done = true;
            }
            catch(InputMismatchException e)
            {
                System.out.println("Well done you messed up try again.");
                kb.next();
                addPet();
            }
        }
    }
    private void addMammal()
    {
        
        boolean done = false;
        while(!done)
        {
            try
            {
                
                System.out.println("What is your pets name?");
                String name = kb.nextLine();
                name = kb.nextLine();
                boolean neutered = mammal.addNeutered();
                System.out.println("What breed is "+name+"?");
                String breed = kb.nextLine();
                System.out.println("What age is "+name+"? in whole years?");
                int age = kb.nextInt();
                if(age<1)
                {
                    while(age<1)
                    {
                        System.out.println("What age is "+name+"? in whole years?");
                        age = kb.nextInt();
                    }
                }
                String [] color = animal.addColors();
                Mammal newMammal = new Mammal(neutered,name,breed, age, color);
                if(mammal.checkIsCopy(newMammal,mammals) == false)
                {
                    mammals.add(newMammal);
                    System.out.println(newMammal.getName() + " added.");
                    done = true;
                }
                else
                {
                    System.out.println(newMammal.getName()+" Appears to be a copy,");
                    done = true;
                }
            }
            catch(InputMismatchException e)
            {
                System.out.println("Very creative, try to break this again, I dare you");
                kb.next();
                addMammal();
            } 
        }
        System.out.println("Mammal added");
    }
    private void addBird()
    {
        boolean done = false;
        while(!done)
        {
            try
            {
                System.out.println("What is your pets name?");
                String name = kb.nextLine();
                name = kb.nextLine();
                double wingspan = bird.addWingspan();
                boolean ableToFly = bird.ableToFly(); 
                System.out.println("What breed is "+name+"?");
                String breed = kb.nextLine();
                System.out.println("What age is "+name+"? in whole years?");
                int age = kb.nextInt();
                if(age<1)
                {
                    while(age<1)
                    {
                        System.out.println("What age is "+name+"? in whole years?");
                        age = kb.nextInt();
                    }
                }
                String [] color = animal.addColors();
                Bird newBird = new Bird(wingspan,ableToFly,name,breed, age, color);
                if(bird.checkIsCopy(newBird,birds) == false)
                {
                    birds.add(newBird);
                    System.out.println(newBird.getName() + " added.");
                    done = true;
                }
                else
                {
                    System.out.println(newBird.getName()+" Appears to be a copy,");
                    done = true;
                }
            }
            catch(InputMismatchException e)
            {
                System.out.println("wow you almost made it, try not breaking it this time");
                kb.next();
                addBird();
            } 
        }
        System.out.println("Bird added");
    }
    private void addFish()
    {
        boolean done = false;
        while(!done)
        {
            
            try
            {
                kb.reset();
                System.out.println("What is your pets name?");
                String name = kb.nextLine();
                name = kb.nextLine();
                String waterType = fish.addWaterType(); 
                System.out.println("What breed is "+name+"?");
                String breed = kb.nextLine();
                System.out.println("What age is "+name+"? in whole years?");
                int age = kb.nextInt();
                if(age<1)
                {
                    while(age<1)
                    {
                        System.out.println("What age is "+name+"? in whole years?");
                        age = kb.nextInt();
                    }
                }
                String [] color = animal.addColors();
                Fish newFish = new Fish(waterType,name,breed, age, color);
                if(fish.checkIsCopy(newFish,fishes) == false)
                {
                    fishes.add(newFish);
                    System.out.println(newFish.getName() + " added.");
                    done = true;
                }
                else
                {
                    System.out.println(newFish.getName()+" Appears to be a copy,");
                    done = true;
                }
                
            }
            catch(InputMismatchException e)
            {
                System.out.println("I'm getting tired of you trying to break my code");
                kb.next();
                addFish();
            } 
        }
        System.out.println("Fish added");
    }

    private void addOwner()
    {
        boolean done = false;
        while(!done)
        {
            try
            {
                System.out.println("Enter Owners name");
                String name = kb.nextLine();
                name = kb.nextLine();
                System.out.println("Enter "+ name +"'s email");
                String email = kb.nextLine();
                System.out.println("Enter "+ name +"'s Phone Number");
                String phoneNum = kb.nextLine();
                System.out.println("Enter "+ name+"'s address");
                String address = kb.nextLine();
                Owner newOwner = new Owner(name,email,phoneNum,address);
                owners.add(newOwner);
                done = true;
            }
            catch(InputMismatchException e)
            {
                System.out.println("If you don't stop trying to break my programm I'm calling the police");
                kb.next();
                addOwner();
            }
        }
    }
    //Continue this
    private void assignPet()
    {
        if(owners.isEmpty())
        {
            System.out.println("Enter an owner before you try to add an animal");
        }
        //if(mammals.isEmpty() || fishes.isEmpty() || birds.isEmpty())
        else
        {
            try
            {
                System.out.println("Enter the number of the owner you like to add to.");
                for(int i = 0; i<owners.size();i++)
                {
                    //make i colored
                    System.out.println( i + "."+owners.get(i).getOwnerName());
                }
                int ownerNum = kb.nextInt();

                System.out.println("What type of pet would you like to add\n1: Mammals \n2: Birds \n3: Fishes");
                int animalClass=kb.nextInt();
                switch(animalClass)
                {
                    case 1:
                        assignMammal(ownerNum);
                        break;
                    case 2:
                        assignBird(ownerNum);
                        break;
                    case 3:
                        assignFish(ownerNum);
                        break;
                }
            }
            catch(InputMismatchException e)
            {

            }
        }
        
    }
    
    
    
    private void editPet()
    {
        try
        {
            //get owner
            printOwnerNames();
            System.out.println("Is one of these your name Y/N");
            String choice = kb.nextLine();
            if(choice.equalsIgnoreCase("y"))
            {
                System.out.println("Enter Your email address");
                String Email = kb.nextLine();
                for(int ownerIndex = 0; ownerIndex>owners.size();ownerIndex++)
                {
                    if(owners.get(ownerIndex).getOwnerEmail().contains(Email))
                    {
                        System.out.println("What type of pet would you like to edit? "
                                + "\nEnter 1 for mammals, Enter 2 for fish, Enter 3 for Birds");
                        int animalClass = 0;
                        animalClass=kb.nextInt();
                        switch(animalClass)
                        {
                            case 1:
                                editMammal(ownerIndex);
                                break;
                            case 2:
                                editFish(ownerIndex);
                                break;
                            case 3:
                                editBird(ownerIndex);
                                break;
                        }
                    }
                    else
                    {
                        System.out.println("Email not found try again.");
                    }
                }
            }
            else if(choice.equalsIgnoreCase("n"))
            {
                System.out.println("Add yourself as an owner, add an unassigned pet and try again");
            }
        }
        catch(InputMismatchException e)
        {
            
        }
    }
    
    private void deletePet()
    {
        try
        {
            //get owner
            printOwnerNames();
            System.out.println("Is one of these your name Y/N");
            String choice = kb.nextLine();
            if(choice.equalsIgnoreCase("y"))
            {
                System.out.println("Enter Your email address");
                String Email = kb.nextLine();
                for(int ownerIndex = 0; ownerIndex<owners.size();ownerIndex++)
                {
                    if(owners.get(ownerIndex).getOwnerEmail().contains(Email))
                    {
                        System.out.println("Owner found");
                        for(int i = 0; i<owners.get(ownerIndex).getPets().size();i++)
                        {
                            System.out.println(i + ": " + owners.get(ownerIndex).getPets().get(i).toString());
                        }
                        System.out.println("Enter the number of the prt you would like to delete");
                        int delete = kb.nextInt();
                        owners.get(ownerIndex).getPets().remove(delete);
                        System.out.println("Pet removed");
                    }
                    else
                    {
                        System.out.println("Email not found try again.");
                    }
                }
            }
            else if(choice.equalsIgnoreCase("n"))
            {
                System.out.println("Add yourself as an owner, add an unassigned pet and try again");
            }
        }
        catch(InputMismatchException e)
        {
            
        }
    }
    
//printing methods
    private void printMenu()
    {
        System.out.println("What Animal Type would you like to print out?\n1: Mammals \n2: Birds \n3: Fishes\n4:print all");
        int choice = kb.nextInt();
        switch(choice)
        {
            case 1: 
                printMammals();
                break;
            case 2:
                printBirds();
                break;
            case 3:
                printFishes();
                break;
            case 4:
                printMammals();
                printBirds();
                printFishes();
        }
    }
    //these are useless
    private void printMammals()
    {
        System.out.println("----------Unassigned Mammals----------");
        printUnassignedMammals();
        System.out.println("-----------Assigned Mammals-----------");
        printAssignedMammals();
    }
    private void printBirds()
    {
        System.out.println("----------Unassigned Birds----------");
        printUnassignedBirds();
        System.out.println("-----------Assigned Birds-----------");
        printAssignedBirds();
    }
    private void printFishes()
    {
        System.out.println("----------Unassigned Fishes----------");
        printUnassignedFish();
        System.out.println("-----------Assigned Fishes-----------");
        printAssignedFish();
    }
    private void printOwnerNames()
    {
        for(int i = 0; i<owners.size();i++)
        {
            System.out.println(i+1+"."+owners.get(i).getOwnerName());
        }
    }
    private void printAssignedPets()
    {
        for(int i  = 0; i<owners.size();i++)
        {
            for(int j = 0; j<owners.get(i).getPets().size();i++)
            {
                System.out.println(owners.get(i).getPets().get(j).toString());
            }
        }
    }
    private void printUnassignedPets()
    {
        System.out.println("-----mammals-----");
        printUnassignedMammals();
        System.out.println("-----birds-----");
        printUnassignedBirds();
        System.out.println("-----Fish-----");
        printUnassignedFish();
        System.out.println("---------------");
    }
    //unassigned stay in this class
    private void printUnassignedMammals()
    {
        for(int i = 0; i<mammals.size();i++)
        {
            System.out.println( i+". " + mammals.get(i).toString());
            System.out.println("---------------------------------------");
        }
    }
    private void printUnassignedBirds()
    {
        for(int i = 0; i<birds.size();i++)
        {
            System.out.println(i+". "+ birds.get(i).toString());
            System.out.println("---------------------------------------");
        }
    }
    
    private void printUnassignedFish()
    {
        for(int i = 0; i<fishes.size();i++)
        {
            System.out.println(i+". "+fishes.get(i).toString());
            System.out.println("---------------------------------------");
        }
    }
    //assigned can be moved
        private void printAssignedMammals()
    {
        for(int i = 0; i<owners.size();i++)
        {
            for(int j = 0; j<owners.get(i).getPets().size();j++)
            {
                if(owners.get(i).getPets().get(j).toString().contains("Type: Mammal"))
                {
                    System.out.println(owners.get(i).getPets().get(j).toString());
                }
            }
        }
    }
    private void printAssignedBirds()
    {
        for(int i = 0; i<owners.size();i++)
        {
            for(int j = 0; j<owners.get(i).getPets().size();j++)
            {
                if(owners.get(i).getPets().get(j).toString().contains("Type: Bird"))
                {
                    System.out.println(owners.get(i).getPets().get(j).toString());
                }
            }
        }
    }
    private void printAssignedFish()
    {
        for(int i = 0; i<owners.size();i++)
        {
            for(int j = 0; j<owners.get(i).getPets().size();j++)
            {
                if(owners.get(i).getPets().get(j).toString().contains("Type: Fish"))
                {
                    System.out.println(owners.get(i).getPets().get(j).toString());
                }
            }
        }
    }
    
    ///edit methods
    private void editMammal(int ownerIndex) {
        System.out.println("Which mammal would you like to edit?");
        for(int i = 0; i<owners.get(ownerIndex).getPets().size();i++)
        {
            if(owners.get(ownerIndex).getPets().get(i).toString().contains("Type: Mammal"))
            {
                System.out.println("Enter "+i+" for " + owners.get(ownerIndex).getPets().get(i).getName());
            }
            //make the user enter only the correct type of pet
        }
        int petIndex = kb.nextInt();
        mammal.editMammal((Mammal) owners.get(ownerIndex).getPets().get(petIndex));
        //remove this
        System.out.println("Mammal edited.");
    }

    private void editFish(int ownerIndex) {
        System.out.println("Which fish would you like to edit?");
        for(int i = 0; i<owners.get(ownerIndex).getPets().size();i++)
        {
            if(owners.get(ownerIndex).getPets().get(i).toString().contains("Type: Fish"))
            {
                System.out.println("Enter "+i+" for " + owners.get(ownerIndex).getPets().get(i).getName());
            }
        }
        int petIndex = kb.nextInt();
        fish.editFish((Fish) owners.get(ownerIndex).getPets().get(petIndex));
        
    }

    private void editBird(int ownerIndex) {
        System.out.println("Which bird would you like to edit?");
        for(int i = 0; i<owners.get(ownerIndex).getPets().size();i++)
        {
            if(owners.get(ownerIndex).getPets().get(i).toString().contains("Type: Bird"))
            {
                System.out.println("Enter "+i+" for " + owners.get(ownerIndex).getPets().get(i).getName());
            }
        }
        int petIndex = kb.nextInt();
        bird.editBird((Bird) owners.get(ownerIndex).getPets().get(petIndex));
    }
    //assign methods
    private void assignFish(int ownerNum) {
        printUnassignedFish();
        System.out.println("Enter the number of the fish you would like added");
        int choice = kb.nextInt();
        owners.get(ownerNum).addToPets(fishes.get(choice));
        System.out.println(fishes.get(choice).getName()+" added.");
        fishes.remove(choice);
    }

    private void assignBird(int ownerNum) {
        printUnassignedBirds();
        System.out.println("Enter the number of the bird you would like added");
        int choice = kb.nextInt();
        owners.get(ownerNum).addToPets(birds.get(choice));
        System.out.println(birds.get(choice).getName()+" added.");
        birds.remove(choice);
    }

    private void assignMammal(int ownerNum) {
        printUnassignedMammals();
        System.out.println("Enter the number of the mammal you woulf like to add");
        int choice = kb.nextInt();
        owners.get(ownerNum).addToPets(mammals.get(choice));
        System.out.println(mammals.get(choice).getName()+" added.");
        mammals.remove(choice);
    }

    private void printingMenu() {
        kb.reset();
        try
        {
            kb.reset();
            int choice = 0;
            System.out.println("Enter 1 to view the printing menu with pets in their natural order."
                    + "\nEnter 2 to view the printing menu with pets in order of gender."
                    + "\nEnter 3 to view printing menu with pets in order of age."); 
            choice = kb.nextInt();
            switch(choice)
            {
                case 1:
                    printMenu();
                    break;
                case 2:
                    System.out.println("A combination of incopetennce and poor time managment made the completion of this method impossible");
                    break;
                case 3:
                    System.out.println("A combination of incopetennce and poor time managment made the completion of this method impossible");
                    break;
            }
        }
        catch(InputMismatchException e)
        {
            
        }
    }

    private void sortByGender() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void sortByAge() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    private void generateStatistics() {
        try
        {
            kb.reset();
            System.out.println("Enter 1 to generate statistics for Mammals.\nEnter 2 to genetate statistics for Birds.\nEnter 3 to generate statistics for Fish");
            int choice = kb.nextInt();
            switch(choice)
            {
                case 1:
                    mammal.mammalStats(owners,mammals);
                    break;
                case 2:
                    bird.birdStats(owners,birds);
                    break;
                case 3:
                    fish.fishStats(owners,fishes);
                    break;
            }
        }
        catch(InputMismatchException e)
        {
            
        }
        
    }
    
    //public Comparetor <Animal> ageComparetor = new Comparetor
}