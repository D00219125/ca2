package OOPCa2AidanBligh;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;
/**
 *
 * @author Aidan Bligh D00219125
 */
public class Mammal extends Animal{
    private Scanner kb = new Scanner(System.in);
    
    private boolean neutered;

    /**
     *Constructor for mammal class
     * @param neutered
     * @param name
     * @param breed
     * @param age
     * @param color
     */
    public Mammal(boolean neutered, String name, String breed, int age, String[] color) {
        super(name, breed, age, color);
        this.neutered = neutered;
    }

    /**
     *Empty constructor for composition
     */
    public Mammal() {
    }
    
    /**
     *Used to add neutered status to mammal, called in constructor 
     * @return
     */
    public boolean addNeutered()
    {
        System.out.println("Is your pet neutered? Enter Y/N");
        String isNeutered = kb.nextLine();
        boolean inputCorrect = false;
        boolean neutered = false;
        while(!inputCorrect)
        {
            if(isNeutered.equalsIgnoreCase("y"))
            {
                neutered = true;
                inputCorrect = true;
            }
            else if(isNeutered.equalsIgnoreCase("n"))
            {
//                neutered = false;
                inputCorrect = true;
            }
            else
            {
                System.out.println("Try again. Enter Y or N");
                addNeutered();
                inputCorrect = true;
            }
        }
        return neutered;
    }

    /**
     *Used to edit mammal
     * @param m
     */
    public void editMammal(Mammal m)
    {
        //menu to ask what values hneed changing
        try
        {
            
            int choice = 0;
            while (choice!=6)
            {
                //add enums
                printEditingOptions();
                switch(choice)
                {
                    case 1:
                        changeName(m);
                        break;
                    case 2:
                        changeAge(m);
                        break;
                    case 3:
                        changeBreed(m);
                        break;
                    case 4:
                        changeColors(m);
                        break;
                    case 5:
                        changeNeutered(m);
                        break;
                }
                
            }
        }
        catch(InputMismatchException e)
        {
            System.out.println("You just had to enter a number...");
        }
    }

    /**
     *Checks if new animal is a copy, call after new animal is constructed
     * @param m
     * @param mammalList
     * @return true if mammal is copy
     */
    public boolean checkIsCopy(Mammal m, ArrayList<Mammal> mammalList)
    {
        boolean isCopy = false;
        for(int i = 0; i>mammalList.size();i++)
        {
            int similarities = 0;
            if(m.getName().equalsIgnoreCase(mammalList.get(i).getName()))
            {
                similarities++;
            }
            if(m.getBreed().equalsIgnoreCase(mammalList.get(i).getBreed()))
            {
                similarities++;
            }
            if(m.getAge() == mammalList.get(i).getAge())
            {
                similarities++;
            }
            if(m.isNeutered() == mammalList.get(i).isNeutered())
            {
                similarities++;
            }
            if(similarities>3)
            {
                isCopy = true;
            }
        }
        return isCopy;
    }
    private void printEditingOptions()
    {
        System.out.println("What attribute would you like to edit?\n1. Name\n2.Age\n3.Breed\n4.Colors\n5.Neutered status");
    }
    
    private void changeName(Mammal m)
    {
        System.out.println("What would you like to rename " + m.getName() + "?");
        String name = kb.nextLine();
        m.setName(name);
        System.out.println("Mammal renamed");   
    }
    private void changeAge(Mammal m)
    {
        System.out.println("What age would you like to set for "+ m.getName() +"?");
        int age = kb.nextInt();
        m.setAge(age);
        System.out.println("new age set");
    }
    
    private void changeBreed(Mammal m)
    {
        System.out.println("What breed would you like to set "+m.getName() + " as?");
        String breed = kb.nextLine();
        m.setBreed(breed);
        System.out.println("New breed set");
    }
    
    private void changeColors(Mammal m)
    {
        String [] color = addColors();
        m.setColor(color);
        System.out.println("colors changed");
    }
    
    private void changeNeutered(Mammal m)
    {
        boolean neutered = addNeutered();
        m.setNeutered(neutered);
        System.out.println("Neutered status changed");
    }

    private void setNeutered(boolean neutered) {
        this.neutered = neutered;
    }

    /**
     *returns true if mammal is neutered
     * @return
     */
    public boolean isNeutered() {
        return neutered;
    }
    
    
    
    @Override
    public String toString() {
        String snipSnip = "neutered";
        if(neutered == false)
        {
            snipSnip = "Not neutered";
        }
        return "Type: Mammal" + "\nneutered: " + snipSnip + "\nname: " + getName() 
                + "\nbreed: " + getBreed() + "\nage: " + getAge() + "\ncolor: "
                + Arrays.deepToString(getColor())+ "\npetId: " + getPetId() /*+ "\nDate Created: " + getDateAndTimeCreated()*/;
    }

    /**
     *generates mammal statistics, prints number of owned and un-owned pets 
     * @param owners
     * @param mammals
     */
    public void mammalStats(ArrayList<Owner> owners, ArrayList<Mammal> mammals)
    {
        int mammalCount = 0;
        int neuteredMammalCount = 0;
        int amountRegistered = 0;
        for(int i = 0; i<owners.size();i++)
        {
           for(int j = 0; j<owners.get(i).getPets().size();j++)
           {
               if(owners.get(i).getPets().get(j).toString().contains("Type: Mammal"))
               {
                   mammalCount++;
                   //can't use isNeutered()
                   if(owners.get(i).getPets().get(j).toString().contains("neutered: neutered"))
                   {
                       neuteredMammalCount++;
                   }
               }
           }
        }
        amountRegistered = mammalCount;
        mammalCount += mammals.size();
        for(int k = 0; k<mammals.size();k++)
        {
            if(mammals.get(k).isNeutered())
            {
                neuteredMammalCount++;
            }
        }
        double percentNeutered = ((double)neuteredMammalCount/mammalCount)*100;
        double percentRegistered = ((double)amountRegistered/mammalCount)*100;
        System.out.println("There are " + mammalCount + " Mammals regietered, "
                + neuteredMammalCount+" of those are neutered making " + 
                percentNeutered + "% of the Mammals in our system neutered."
                        + "\nOut of " + mammalCount + "only " + amountRegistered +
                " have an owner on our  system. That adds up to " + percentRegistered 
                    + "% being registered");
    }
    
}
